<div class="form-group {{ $errors->has('ordre') ? 'has-error' : ''}}">
    <label for="ordre" class="control-label">{{ 'Ordre' }}</label>
    <input class="form-control" name="ordre" type="number" id="ordre" value="{{ isset($personne->ordre) ? $personne->ordre : ''}}" >
    {!! $errors->first('ordre', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nom') ? 'has-error' : ''}}">
    <label for="nom" class="control-label">{{ 'Nom' }}</label>
    <input class="form-control" name="nom" type="text" id="nom" value="{{ isset($personne->nom) ? $personne->nom : ''}}" >
    {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('parent_id') ? 'has-error' : ''}}">
    <label for="parent_id" class="control-label">{{ 'Parent Id' }}</label>
    <select name="parent_id" class="form-control" id="parent_id" >
    @if(isset($personnes))
        @foreach($personnes as $personne)
        <option value="">---Choisir le parent---</option>
        <option value="{{ $personne->id}}">{{ $personne->nom}}</option>
        @endforeach
    @endif
</select>
    {!! $errors->first('parent_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
