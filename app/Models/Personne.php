<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Personne extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'personnes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['ordre', 'nom', 'parent_id'];

    
    public static function getAllNameOfParents(){
        $users = DB::table('personnes')
            ->join('personnes p', 'p.id', '=', 'personnes.id')
            ->select('personnes.*','p.nom noms')
            ->get();
        return $users;
    }

    
}
